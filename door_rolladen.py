#!/usr/bin/env python
# -*- coding: utf8 -*-

#import shelve
import csv
from pyA20.gpio import gpio
from pyA20.gpio import port
import MFRC522
import os
import signal
from time import sleep, time , ctime
import random 

OPEN = 1
CLOSED = 0
RELAIS = 203 # c.f. pcb-layout and  http://wiki.friendlyarm.com/wiki/index.php/NanoPi_Duo#Layout
SENSOR = port.PA4

continue_reading = True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
def door(dstate):
    gpio.output(RELAIS,dstate)
    # since the beeper does not work at the moment torture the relais a bit
def beep(beeplength):
    for i in range(beeplength):
        gpio.output(RELAIS,OPEN)  
        sleep(0.05)
        gpio.output(RELAIS, CLOSED)
        sleep(0.05)
def rolladen(leftup, leftdown, rightup, rightdown, direction):
    var = random.randint(0,3)
    gpio.output(leftup, LOW)
    gpio.output(leftdown, LOW)
    gpio.output(rightup, LOW)
    gpio.output(rightdown, LOW)

    if var == 0:
	pin = leftup
    if var == 1:		
        pin = leftdown
    if var == 2:
        pin = rightup
    else:
        pin = rightdown
    
    gpio.output( pin, HIGH)
    sleep(2)
    gpio.output( pin, LOW)
    sleep(2)


     

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)
# configure gpio
gpio.init()
# relais:
gpio.setcfg(RELAIS, gpio.OUTPUT)
door(CLOSED)
# beeper:
gpio.setcfg(SENSOR,gpio.INPUT)
gpio.pullup(SENSOR,gpio.PULLUP) # now there is also a smd pullup
# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()
# Welcome message
print "RFID Reader started"
print "Press Ctrl-C to stop."
admin = 0
lastopen = 0
admintime = 0
registermode = 0
# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:
    # there is a manual switch connected to pin 10
    if gpio.input(SENSOR)==0:
        rol = random.randint(0,3)
        print " rolladen "
	print rol
        rolladen( port.PL11, port.PA11, port.PA12, port.PG6 ,rol )
        with open ( '/root/logfile', 'a+') as logfile:
            writer = csv.writer(logfile)
            writer.writerow( [ctime() , " rolladen"])

    # Scan for cards
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        print "Card detected"

    # Get the UID of the card
    (status,uid) = MIFAREReader.MFRC522_Anticoll()

    # If we have the UID, continue
    if registermode and time() > admintime +10:
        registermode = False
        print " exiting register mode"
    if status == MIFAREReader.MI_OK:

        UID=str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
        # check for register mode:
        # MFRCC gets strange reads if reading twice in one while loop
        print time()
        print lastopen
        print admin
        if registermode:
            if oldUID != UID:
                # dooraccess : name , UID , valid until date , registered date
                newentry = [ card [ 0 ] + "_guest_key" , UID , time ( ) + 3600*24*14 , ctime()]
                dooraccess.append( newentry )
                print newentry
                with open ( '/root/dooraccess' , 'w' ) as writefile:
                    writer = csv.writer ( writefile )
                    writer.writerows( dooraccess )
                #os.system('sync_doors.sh')
                beep(8)
                oldUID = UID
            lastopen = time()
        elif time() < lastopen + 3 and admin and oldUID == UID:
            # if admin card is read for longer than 5 seconds a second card can be registered
            print "entering register mode"
            beep(2)
            sleep(0.125)
            beep(2)
            admintime = time()
            registermode = True

        else:
            # Print UID
            print "Card read UID: "+UID

            # open csv on each request to have recent database
            with open ('/root/dooraccess' , 'r' ) as readfile:
                reader = csv.reader ( readfile )
                dooraccess = list(reader)
            with open ('/root/dooradmin'  , 'r' ) as readfile:
                reader = csv.reader ( readfile )
                dooradmin = list(reader)

            # check for the UID in the two tables
            for line in dooraccess:
                if line [2] < time():
                    dooraccess.remove(line)
                    # manifest that in the file, otherwiese it gets spammed.
                    with open ('/root/dooraccess' , 'w' ) as writefile:
                        writer = csv.writer ( writefile )
                        writer.writerows(dooraccess)
                    continue;
                if line [1] == UID:
                    card = line
                    break;
                else: card = False
            admin = False
            for line in dooradmin:
                if line [ 1 ] == UID:
                    card = line
                    admin = True
            if card:
                print card [ 0 ] # thats the name !
                with open ( '/root/logfile' , 'a+' ) as logfile:
                    writer = csv.writer(logfile)
                    if admin:
                        writer.writerow( [ ctime ( )  , "admin" ] )
                    else:
                        writer.writerow( [ ctime ( )  , card [ 0 ] ] )

                if admin : print "admin"
                door(OPEN)
                sleep(5)
                door(CLOSED)
                print "door closed"
                lastopen = time()
                oldUID=UID
            else:  # log cards that do not match, i.e. overdue guestcards
                with open ( '/root/attempted' , 'a+' ) as attempted:
                    writer = csv.writer(attempted)
                    writer.writerow( [ ctime() , UID ] )
    sleep(0.2)
        # This is the default key for authentication
        #key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

        # Select the scanned tag
        #MIFAREReader.MFRC522_SelectTag(uid)

        # Authenticate
        #status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

        # Check if authenticated
        #if status == MIFAREReader.MI_OK:
        #    MIFAREReader.MFRC522_Read(8)
        #    MIFAREReader.MFRC522_StopCrypto1()
        #else:
        #    print "Authentication error"



