#!/bin/bash


# audio amp enable
echo "11" > /sys/class/gpio/export
sleep 1 ;  # Short delay while GPIO permissions are set up
echo "out" > /sys/class/gpio/gpio11/direction
echo "0" > /sys/class/gpio/gpio11/value
# door bell button 198  and door open wheel
echo "12" > /sys/class/gpio/export
sleep 1 ; #  Short delay while GPIO permissions are set up
echo "in" > /sys/class/gpio/gpio198/direction
# hang up earpiece sensor 
echo "198" > /sys/class/gpio/export
sleep 1 ; # Short delay while GPIO permissions are set up
echo "in" > /sys/class/gpio/gpio12/direction
# relais
echo "203" > /sys/class/gpio/export
sleep 1 ; # Short delay while GPIO permissions are set up
echo "out" > /sys/class/gpio/gpio203/direction
echo "0" > /sys/class/gpio/gpio203/value

