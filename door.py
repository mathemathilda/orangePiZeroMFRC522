#!/usr/bin/env python
# -*- coding: utf8 -*-

#import shelve
import csv
from pyA20.gpio import gpio
from pyA20.gpio import port
import MFRC522
import os
import signal
from time import sleep, time , ctime

OPEN = 1
CLOSED = 0
RELAIS = 203 # c.f. pcb-layout and  http://wiki.friendlyarm.com/wiki/index.php/NanoPi_Duo#Layout
SENSOR = 198
BEEPER = 363

continue_reading = True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print ("Ctrl+C captured, ending read.")
    continue_reading = False

def door(dstate):
    gpio.output(RELAIS,dstate)

def beep(beep_count):
    # since the beeper does not work at the moment torture the relais a bit
    for i in range(beep_count):
        gpio.output(RELAIS,OPEN)  
        sleep(0.05)
        gpio.output(RELAIS, CLOSED)
        sleep(0.05)

def manual_open():
    door(OPEN)
    print ("manual open")
    sleep(3)
    with open ( '/root/logfile', 'a+') as logfile:
        writer = csv.writer(logfile)
        writer.writerow( [ctime() , " manual open"])
    door(CLOSED)

def register(admin_name, UID):
    print(" admin name: " , admin_name)
    if not check_access(UID)[0]:
        # dooraccess : name , UID , valid until date , registered date
        newentry = [ admin_name  + "_guest_key" , UID , time() + 3600*24*14 , ctime()]
        print (newentry)
        with open ( '/root/dooraccess' , 'a+' ) as writefile:
            writer = csv.writer ( writefile )
            writer.writerow( newentry )
        #os.system('sync_doors.sh')
        beep(8)



def write_access_to_logfile(name, is_admin):
    with open ( '/root/logfile' , 'a+' ) as logfile:
        writer = csv.writer(logfile)
        if is_admin:
            writer.writerow( [ ctime ( )  , "admin" ] )
        else:
            writer.writerow( [ ctime ( )  , name ] )
    
def remove_expired_dooraccess_entries():
    with open ('/root/dooraccess' , 'r' ) as readfile:
        reader = csv.reader ( readfile )
        dooraccess = list(reader)
    for line in dooraccess:
        if float(line [2]) < time():
            dooraccess.remove(line)
            with open ('/root/dooraccess' , 'w' ) as writefile:
                writer = csv.writer ( writefile )
                writer.writerows(dooraccess)

def check_access(UID):
    # open csv on each request to have recent database
    with open ('/root/dooraccess' , 'r' ) as readfile:
        reader = csv.reader ( readfile )
        dooraccess = list(reader)
    with open ('/root/dooradmin'  , 'r' ) as readfile:
        reader = csv.reader ( readfile )
        dooradmin = list(reader)

    has_access = False
    is_admin = False
    name=""
    # check for the UID in the two tables
    for line in dooraccess:
        if line [1] == UID:
            name = line[0]
            has_access = True
    for line in dooradmin:
        if line [ 1 ] == UID:
            name =  line[0]
            is_admin = True
            has_access = True
    return (name, has_access, is_admin)

def open_door(name, is_admin):
    print (name) 
    write_access_to_logfile(name, is_admin)
    if is_admin : print ("admin")
    door(OPEN)
    sleep(5)
    door(CLOSED)
    print ("door closed")

def log_attempted(UID):
    with open ( '/root/attempted' , 'a+' ) as attempted:
        writer = csv.writer(attempted)
        writer.writerow( [ ctime() , UID ] )

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)
# configure gpio
gpio.init()
# relais:
gpio.setcfg(RELAIS, gpio.OUTPUT)
door(CLOSED)
# beeper:
gpio.setcfg(BEEPER, gpio.OUTPUT)
gpio.setcfg(SENSOR,gpio.INPUT)
gpio.pullup(SENSOR,gpio.PULLUP) # now there is also a smd pullup
# Welcome message
print ("RFID Reader started")
print ("Press Ctrl-C to stop.")
lastopen = 0
registermode = 0

# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:
    sleep(0.4)
    # there is a manual switch connected to pin 10
    if gpio.input(SENSOR)==0:
        manual_open()

    # Create an object of the class MFRC522
    MIFAREReader = MFRC522.MFRC522()
    # Scan for cards
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        print ("Card detected")

        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()
    
        # If we have the UID, continue
        if status == MIFAREReader.MI_OK:
    
            UID=str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
            #print (time())
            #print (lastopen)
            print ("Card read UID: "+UID)
            (name, has_access, is_admin) = check_access(UID)
            
            if registermode:
                register(admin_name, UID)
                lastopen = time()
            elif time() < lastopen + 3 and is_admin:
                registermode = True
                register_mode_start_time = time()
                admin_name=name
                print ("entering register mode")
                beep(4)
    
            else:
                if has_access:
                    open_door(name, is_admin)
                    lastopen = time()
                else:  
                    log_attempted(UID)
                #remove_expired_dooraccess_entries()
        if registermode and time() > register_mode_start_time +10:
            registermode = False
            print (" exiting register mode")
        # This is the default key for authentication
        #key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

        # Select the scanned tag
        #MIFAREReader.MFRC522_SelectTag(uid)

        # Authenticate
        #status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

        # Check if authenticated
        #if status == MIFAREReader.MI_OK:
        #    MIFAREReader.MFRC522_Read(8)
        #    MIFAREReader.MFRC522_StopCrypto1()
        #else:
        #    print ("Authentication error")



