#!/bin/bash
# attatch ncat listening ports, posbly more than one
watchfile=/sys/class/gpio/gpio12/value
state=0
while true; do
  if [ $state != "$(cat $watchfile)" ]; then
    state=$(( ($state+1)%2))
    if [ $state == "1" ]; then
	if ps -e | grep [b]aresip ; then
               killall baresip
        fi
        ssh pi@10.6.26.61 './launch_sip.sh'
	# enable remote speaker
        ssh pi@10.6.26.61 'echo "1" > /sys/class/gpio/gpio11/value'
	# disable local amplifier
	echo "0" > /sys/class/gpio/gpio11/value
	pactl set-source-volume  5 20%
        ssh pi@10.6.26.61 'pactl set-source-volume  5 60%'
	baresip -e "d phone@gegensprech.lan" &
	echo " establish call"
    else 
        killall baresip 
	echo "hang up"
	echo "1" > /sys/class/gpio/gpio11/value
	#disable remote speaker
        ssh pi@10.6.26.61 'echo "0" > /sys/class/gpio/gpio11/value'

    fi
  fi
  sleep 0.1
done


