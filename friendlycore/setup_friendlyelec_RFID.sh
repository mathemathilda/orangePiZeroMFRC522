#!/bin/bash
# get this scriipt with 
# git clone https://gitlab.com/mathemathilda/orangePiZeroMFRC522.git

#clock down cpu:

echo "ENABLE=true \n MIN_SPEED=240000 \n MAX_SPEED=480000 \n GOVERNOR=ondemand" > /etc/default/cpufrequtils
/etc/init.d/cpufrequtils restart

# check cpu with 
# cpufreq-info
#available cpu freqs:
# cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies
# check what takes all the time to boot:
# systemd-analyze blame
# have a look at active jobs
# systemctl list-jobs
# system still runs very hot and does not reboot fine
#maybe checkout arch:
# https://github.com/arnarg/nanopi-duo-arch-linux-build
# for me it throws al linker error at the moment ...
# the image supplied by friendlyelec seems o be the goto


# setup spidev:
# there is a readme that basically offers the options we have
#vim /boot/dtb-X.X.XX-sunxi/
# add "spi-spidev" to overlays
#vim /boot/armbianENV.txt
#echo "param_spidev_spi_bus=1" >> /boot/armbianEnv.txt

# reboot the system, shut down with 
#sudo halt
# wait until it stops blinking and then cut the power to reboot



# you need to change the spi device from 0 to 1
npi-config

apt update

apt install python3-pip python3-dev locate lm-sensors
#optional:
apt install 
pip3 install setuptools==58.2.0
pip3 install wheel
pip3 install spidev

#python gpio library
git clone https://github.com/karabek/nanopi_duo_gpio_pyH3
cd nanopi_duo_gpio_pyH3
python setup.py install
cd ..

cd ~/
#  python SPI for the MFRC522 reader
git clone https://github.com/lthiery/SPI-Py.git
cd SPI-Py
python setup.py install
cd ..

 


# to find out which pin is which go to http://linux-sunxi.org/GPIO, there is a description...

# install initdoor to launch door.py at bootup

cp /root/orangePiZeroMFRC522/openwrt/doorinitscript_initd /etc/init.d/
update-rc.d doorinitscript_initd defaults

touch /root/dooradmin
touch /root/dooraccess

# followinng for debugging:

